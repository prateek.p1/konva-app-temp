import React from 'react';
import { Layer, Image as KonvaImage } from 'react-konva'
import useImage from 'use-image';
import { AppContext } from '../../App'

export default () => {

  const {
    stageRef,
    konvaImageSrcUrl
  } = React.useContext(AppContext);

  const [image, status] = useImage(konvaImageSrcUrl, 'anonymous');

  if (!stageRef || !stageRef.current)
    return <></>

  if (status !== 'loaded')
    return <></>

  return (
    <Layer id='konva-image-layer'>

      <KonvaImage
        id='konva-image'
        image={image}
        x={0}
        y={0}
        width={stageRef.current.width()}
        height={stageRef.current.height()}
      />

    </Layer>
  )
}