import React from "react"
import { Stage } from "react-konva"
import { AppContext } from "../App"
import ImageLayer from "./ImageLayer/ImageLayer";

export default () => {

  const { stageRef } = React.useContext(AppContext);

  return (
    <Stage
      id='konva-stage'
      ref={stageRef}
      width={window.innerWidth - 300}
      height={window.innerHeight - 300}
      className='border-4 border-slate-800 dark:border-slate-300'
    >
      <ImageLayer />
    </Stage>
  )
}