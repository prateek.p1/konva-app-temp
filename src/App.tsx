import Konva from 'konva'
import React from 'react'
import './App.css'
import ImageUpload from './ImageUpload/ImageUpload'
import KonvaApp from './KonvaApp/KonvaApp'

export const AppContext = React.createContext<AppContext>({} as AppContext)

function App() {

  const stageRef = React.useRef<Konva.Stage>(undefined as any)
  const [konvaImageSrcUrl, setKonvaImageSrcUrl] = React.useState<string>('')

  const memo = {
    stageRef,
    konvaImageSrcUrl,
    setKonvaImageSrcUrl
  }

  return (
    <AppContext.Provider value={memo}>
      <ImageUpload />
      <KonvaApp />
    </AppContext.Provider>
  )
}

export default App


type AppContext = {
  stageRef: React.MutableRefObject<Konva.Stage>,
  konvaImageSrcUrl: string,
  setKonvaImageSrcUrl: React.Dispatch<React.SetStateAction<string>>,
}
